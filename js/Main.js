import * as THREE from 'https://cdn.skypack.dev/three@0.129';
import { FirstPersonControls } from 'https://cdn.skypack.dev/three@0.129/examples/jsm/controls/FirstPersonControls.js';

class Main {
    constructor() {
        this.camera = null;
        this.scene = null;
        this.renderer = null;
        this.controls = null;
        this.pointer = null;

        this.init();
        this.loadImages();
        this.listener();
        this.animate();
    }

    init() {
        this.initCamera();
        this.initScene();
        this.initRenderer();
        this.initControl();
    }

    initCamera() {
        this.camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
    }

    initScene() {
        this.scene = new THREE.Scene();
    }

    initRenderer() {
        this.renderer = new THREE.WebGLRenderer( { antialias: true } );
        this.renderer.setClearColor( 0x000, 1 );
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( this.renderer.domElement );
    }

    initControl() {
        this.pointer = new THREE.Vector2();
        this.controls = new FirstPersonControls( this.camera );
        this.controls.activeLook = true;
        this.controls.movementSpeed = 10;
        this.controls.noFly = true;
        this.controls.lookVertical = true;
        this.controls.constrainVertical = true;
    }

    positionByDeg(r, latitude, longitude) {
        let x = r * Math.sin(this.degToRad(longitude)) * Math.cos(this.degToRad(latitude));
        let y = r * Math.sin(this.degToRad(longitude)) * Math.sin(this.degToRad(latitude));
        let z = r * Math.cos(this.degToRad(longitude));

        return [x, y, z];
    }

    loadImages() {
        // add images here,
        // latitude and longitude can be randomized
        const images = [
            {
                src: './textures/normal/01.jpeg',
                hoverSrc: './textures/normal/01.jpeg',
                latitude: 0,
                longitude: 0,
            },
            {
                src: './textures/normal/02.jpeg',
                hoverSrc: './textures/normal/02.jpeg',
                latitude: 20,
                longitude: 45,
            },
            {
                src: './textures/normal/03.jpeg',
                hoverSrc: './textures/normal/03.jpeg',
                latitude: 20,
                longitude: 90,
            },
            {
                src: './textures/normal/04.jpeg',
                hoverSrc: './textures/normal/04.jpeg',
                latitude: -30,
                longitude: 135,
            },
            {
                src: './textures/normal/05.jpeg',
                hoverSrc: './textures/normal/05.jpeg',
                latitude: 0,
                longitude: 180,
            },
            {
                src: './textures/normal/06.jpeg',
                hoverSrc: './textures/normal/06.jpeg',
                latitude: 0,
                longitude: 225,
            },
            {
                src: './textures/normal/07.jpeg',
                hoverSrc: './textures/normal/07.jpeg',
                latitude: 0,
                longitude: 270,
            },
            {
                src: './textures/normal/08.jpeg',
                hoverSrc: './textures/normal/08.jpeg',
                latitude: 0,
                longitude: 315,
            }
        ];

        const loader = new THREE.TextureLoader();

        images.forEach((image) => {
            loader.load(image.src, (texture) => {
                const geometry = new THREE.BoxGeometry(texture.image.width, texture.image.height, 1);
                const material = new THREE.MeshBasicMaterial( { map: texture } );

                let mesh = new THREE.Mesh( geometry, material );
                const [x, y, z] = this.positionByDeg(-3000, image.latitude, -image.longitude);
                mesh.position.x = x;
                mesh.position.y = y;
                mesh.position.z = z;
                mesh.rotation.y = Math.atan2( ( this.camera.position.x - mesh.position.x ), ( this.camera.position.z - mesh.position.z ) );
                this.scene.add( mesh );
            })
        })
    }

    listener() {
        window.addEventListener( 'resize', this.onResize.bind(this) );
        this.renderer.domElement.addEventListener( 'pointermove', this.onPointerMove.bind(this) );
    }

    onResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();

        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize( window.innerWidth, window.innerHeight );
    }

    onPointerMove( e ) {
        this.pointer.x = e.clientX;
        this.pointer.y = e.clientY;
    }

    degToRad(deg) {
        return deg * Math.PI / 180;
    }

    animate() {
        requestAnimationFrame(this.animate.bind(this));

        this.render();
    }

    pick() {
        this.camera.setViewOffset(
            this.renderer.domElement.width,
            this.renderer.domElement.height,
            this.pointer.x * window.devicePixelRatio | 0,
            this.pointer.y * window.devicePixelRatio | 0,
            1000,
            1000
        );

        // render the scene
        // this.renderer.setRenderTarget( this.pickingTexture );
        // this.renderer.render( this.pickingScene, this.camera );

        // clear the view offset so rendering returns to normal

        this.camera.clearViewOffset();
    }

    render() {
        this.controls.update(0.1);
        // this.camera.position.x = 0;
        // this.camera.position.z = 0;

        // this.pick();

        this.renderer.setRenderTarget( null );
        this.renderer.render(this.scene, this.camera);
    }
}

export default Main;
